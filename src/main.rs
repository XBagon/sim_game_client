extern crate bincode;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate byteorder;

use std::net::TcpStream;
use bincode::{serialize, deserialize};
use serde::Serialize;
use std::io::{Read,Write};
use byteorder::{ReadBytesExt,WriteBytesExt,LittleEndian};

fn main() {
    let mut connection = TcpStream::connect("127.0.0.1:1234").unwrap();

    loop {
        send_net_package(&mut connection, NetRequest::Deposit(1));
        let size = connection.read_u32::<LittleEndian>().unwrap();
        //let mut buffer = Vec::<u8>::with_capacity(size as usize);
        let mut buffer = vec![0u8; size as usize];
        connection.read_exact(&mut buffer).unwrap();
        let result : NetResultReceive = deserialize(&buffer[..]).unwrap();
        println!("{:?}",result);
        buffer.clear();
    }
}

fn send_net_package<T>(connection :&mut TcpStream, request : T) where T: Serialize{
    let content = &serialize(&request).unwrap()[..];
    connection.write_u32::<LittleEndian>(content.len() as u32);
    connection.write_all(content);
}

#[derive(Serialize, Deserialize, Debug)]
struct Position {
    x: f64,
    y: f64
}

#[derive(Serialize, Deserialize, Debug)]
struct Deposit {
    material : u64,
    richness : f64,
    amount : f64
}

#[derive(Serialize, Deserialize, Debug)]
enum NetRequest{
    Position(u32),
    Deposit(u32)
}

#[derive(Serialize, Debug)]
enum NetResultSend<'a>{
    Position(&'a Position),
    Deposit(&'a Deposit)
}

#[derive(Deserialize, Debug)]
enum NetResultReceive{
    Position(Position),
    Deposit(Deposit)
}